<?php
session_start();

// echo "<pre>";
// print_r($_SESSION);
// echo "</pre>";
if(isset($_GET['logout']))
{
    session_destroy();
    header('Location: index.php');
    exit;
}
if(!isset($_SESSION['logado']))
{
    header('Location: index.php');
    exit;
}

?>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Área Administrativa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
</head>
<body>

    <div class="container mt-3">
        <div class="row">
            <div class="col-12">
                <h1>Área Administrativa</h1>
                <p>Olá, hoje é dia <?php echo date("d/m/Y"); ?></p>
                <p><a href="admin.php?logout" class="btn btn-primary">Sair</a></p>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>